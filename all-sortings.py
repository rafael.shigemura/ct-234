from time import time
import bubblesort
import selectionsort
import insertionsort
import mergesort
import quicksort
import heapsort


if __name__ == "__main__":
    n = 100000
    #arr = bubblesort.inicializa_array_seq(n)
    arr, aux = mergesort.inicializa_array(n)

    t_ini = time()
    arr_ord = bubblesort.bubblesort(n, arr)
    t_fim = time() - t_ini
    print("BubbleSort de %d elementos calculado em %.6f segundos"%(n,t_fim))

    t_ini = time()
    arr_ord = selectionsort.selectionsort(n, arr)
    t_fim = time() - t_ini
    print("SelectionSort de %d elementos calculado em %.6f segundos"%(n,t_fim))

    t_ini = time()
    arr_ord = insertionsort.insertionsort(n, arr)
    t_fim = time() - t_ini
    print("InsertionSort de %d elementos calculado em %.6f segundos"%(n,t_fim))

    t_ini = time()
    arr_ord = quicksort.quicksort(arr, 0, n-1)
    t_fim = time() - t_ini
    print("QuickSort de %d elementos calculado em %.6f segundos"%(n,t_fim))

    t_ini = time()
    arr_ord = mergesort.mergesort(arr, aux, 0, n-1)
    t_fim = time() - t_ini
    print("MergeSort de %d elementos calculado em %.6f segundos"%(n,t_fim))

    t_ini = time()
    arr_ord = heapsort.heapsort(arr)
    t_fim = time() - t_ini
    print("HeapSort de %d elementos calculado em %.6f segundos"%(n,t_fim))