import numpy
import math
from time import time


arr = None
v = None
existe = False
pos = -1


def inicializa_array(n):
    return numpy.sort(numpy.random.randint(n, size=n))
    #return [i for i in range(0,n-1)]


def buscabin(ini, fim):
    global v
    global existe
    global pos

    if (fim-ini) == 1: return # NÃO ENCONTROU EXATO...
    if (v==arr[ini + math.floor((fim - ini) / 2)]): 
        existe = True
        pos = ini + math.floor((fim - ini) / 2)
    if (v<arr[ini + math.floor((fim - ini) / 2)]): 
        buscabin(ini, ini + math.floor((fim - ini) / 2))
    else:
        buscabin(ini + math.floor((fim - ini) / 2), fim)



if __name__ == "__main__":
    n = 100000
    v = 9997
    arr = inicializa_array(n)
    t_ini = time()
    buscabin(0, n-1)
    t_fim = time() - t_ini
    print("O valor %d tem existência %s no vetor na posição %d. Busca realizada em %.0f segundos"%(v,existe,pos,t_fim))
    pass