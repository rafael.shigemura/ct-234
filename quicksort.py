import copy
from math import floor
from time import time
import numpy


global va
global v
global aux


def inicializa_array(n, seq=False, val=None):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    if seq: v = [i for i in range(n)]
    elif val: v = val
    else: v = numpy.random.randint(n, size=n)
    va = copy.deepcopy(v)


def particao(A, esquerda, direita):
    # 1. Seleção do pivô. O pivô será o 1o elemento A[esquerda].
    pivo = A[esquerda]

    # Particionamento do vetor em duas metades.
    i = esquerda + 1
    j = direita

    # Loop infinito: a garantia de parada está no desenho do algoritmo
    while (True):

        # Itera pelo vetor da ESQ p/ DIR enquanto o elemento A[i] 
        # for MENOR que o pivo E i não atingir o FIM do vetor.
        # Isso existe para localizar o valor MAIOR OU IGUAL ao pivo
        # que posteriormente será trocado com o MENOR.
        while (A[i] < pivo and i < direita) : i += 1

        # Itera pelo vetor da DIR p/ ESQ enquanto o elemento A[i] 
        # for MAIOR que o pivo e i não chegar ao INICIO do vetor
        # Isso existe para localizar o valor MENOR ao pivo
        # que posteriormente será trocado com o MAIOR.
        while (A[j] >= pivo and j > esquerda) : j -= 1

        # Ponteiros i e j se cruzaram. Caso positivo, retorne o índice do pivo
        if i >= j: break

        # Troca elementos encontrados acima de lugar.
        A[i], A[j] = A[j], A[i]

    # Coloca o pivo no lugar certo.
    A[esquerda], A[j] = A[j], pivo

    # j é o índice em que o pivo agora está.
    return j


def quicksort(A, esquerda, direita):
    if esquerda < direita:
        indice_pivo = particao(A, esquerda, direita)
        # 3. Ordenação recursiva das partes do arranjo.
        quicksort(A, esquerda, indice_pivo - 1)
        quicksort(A, indice_pivo + 1, direita)


if __name__ == "__main__":
    n = 4
    inicializa_array(n, val=[7, 9, 0, 8])
    #inicializa_array(n)
    t_ini = time()
    quicksort(v, 0, n-1)
    t_fim = time() - t_ini
    print("QuickSort de %s em %s calculado em %.6f segundos "%(va, v, t_fim))
    #print("QuickSort de %d elementos calculado em %.6f segundos "%(n, t_fim))
    pass