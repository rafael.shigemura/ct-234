from time import time


def fib(n):
    if(n==0 or n==1): return 1
    return fib(n-1) + fib(n-2)


if __name__ == "__main__":
    n = 40
    t_ini = time()
    n_fib = fib(n)
    t_fim = time() - t_ini
    print("Fibo rec de %d é %d calculado em %.0f segundos"%(n,n_fib,t_fim))
