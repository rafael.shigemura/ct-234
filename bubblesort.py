from time import time
import numpy
import copy

global v 

def inicializa_array(n):
    return numpy.random.randint(n, size=n)

def inicializa_array_seq(n):
    return [i for i in range(n)]

def bubblesort(n, arr):
    srt_arr = copy.deepcopy(arr)
    exch = False  # FLAG DE TROCA PARA VERIFICAR SE O VETOR ESTÁ ORDENADO
    for i in range(n):                      # LAÇO EXTERNO PARA PERCORRER O VETOR DE 0 A n-1                              
        for j in range(n-i-1):              # LAÇO INTERNO PARA PERCORRER A PORÇAO DO VETOR QUE AINDA NÃO ESTÁ ORDENADA, DE 0 A (n-i) - 1
            if srt_arr[j] > srt_arr[j+1]:   # SE O VALOR EM j FOR MAIOR QUE O VALOR NA PROXIMA POSIÇÃO, DEVERÁ SER TROCADO
                exch = True                 # ATIVA FLAG DE TROCA, O VETOR NÃO ESTÁ ORDENADO
                aux = srt_arr[j]            # AUXILIAR RECEBE O ANTERIOR (NOVO MAIOR)
                srt_arr[j] = srt_arr[j+1]   # POSIÇÃO DO ANTIGO MENOR RECEBE O NOVO MENOR
                srt_arr[j+1] = aux          # POSIÇÃO DO ANTIGO MAIOR RECEBE O NOVO MAIOR
        if not exch:                        # VERIFICA SE VETOR ORDENADO. SE ESTIVER, ABORTA
            print("Bubblesort em array ordenado. Abortando")
            break
    return srt_arr

if __name__ == "__main__":
    n = 10000
    arr = inicializa_array(n)
    t_ini = time()
    arr_ord = bubblesort(n,arr)
    t_fim = time() - t_ini
    print("Bubblesort de %d elementos calculado em %.0f segundos"%(n,t_fim))
    pass