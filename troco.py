from time import time


def DPMakeChange(moedas, troco):
    quant = [None for i in range(troco+1)]
    ultima = [None for i in range(troco+1)]

    quant[0] = 0
    ultima[0] = 0

    for cents in range(1, troco+1):
        quantProv = cents
        ultProv = 1
        for j in range(len(moedas)):
            if moedas[j] > cents: continue                
            if quant[cents - moedas[j]] + 1 < quantProv:
                quantProv = quant[cents - moedas[j]] + 1
                ultProv = moedas[j]
        quant[cents] = quantProv
        ultima[cents] = ultProv
    
    return quant[troco]


if __name__ == "__main__":
    moedas = [1,5,10]
    troco = 15
    t_ini = time()
    res = DPMakeChange(moedas, troco)
    t_fim = time() - t_ini
    print("Qtd otima de moedas de troco de %d com moedas %s é %d"%(troco,str(moedas),res))
