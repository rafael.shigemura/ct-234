from time import time

def pmbf(T,p):
    n = len(T)
    m = len(p)
    ops = 0
    for i in range(n-m):
        j=0
        while j<m and T[i+j]==p[j]:
            j+=1
            ops+=1
        if j==m:
            return i,ops
        ops+=1
    return -1,ops


if __name__ == "__main__":

    T = "bacbacbab"
    p = "baa"

    t_ini = time()
    ix,ops = pmbf(T,p)
    t_fim = time() - t_ini
    print("A palavra %s encontrada no texto %s no indice %d. Algoritmo realizado em %.0f segundos com %d operações"%(p,T,ix,t_fim,ops))
    pass