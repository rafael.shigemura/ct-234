from time import time
import numpy
import random

global arr 

def inicializa_array(n, alfa):
    seq = ""
    for _ in range(n):
        seq += str(random.choice(alfa))
    return seq

def brute_force(T, P):
    l = 0
    n = len(T)                  # CONTA TAM TEXTO
    m = len(P)                  # CONTA TAM PALAVRA
    for i in range(n-m+1):
        j = 0
        while j < m:            # WHILE MODIFICADO PARA CONTAR COMPARACOES TOTAIS
            if T[i+j] == P[j]:
                j += 1
                l += 1          # CONTA COMPARACAO QUANDO DER MATCH
            else:
                l += 1          # E CONTA QUANDO NÃO DER MATCH
                break
        if j == m:
            return i,l
    return -1,l


def AFMatch(T, P, AF, alfa):
    s = 0
    n = len(T)                  # CONTA TAM TEXTO
    m = len(P)                  # CONTA TAM PALAVRA
    for i in range(n):
        s = AF[s,alfa.index(T[i])]
        if s == m:
            return i-m+1
    return -1


def AFConstruct(P, alfa):
    m = len(P)
    a = len(alfa)
    AF = numpy.zeros((m,a), dtype=int)
    for s in range(m):
        for x in alfa:
            k = min(s+2, m+1)
            while True:
                k -= 1
                if P[:k-1] != P[:s-1]:
                    break
            AF[s,alfa.index(x)] = k
    return AF



if __name__ == "__main__":
    bases = "abc"
    #T = inicializa_array(10, bases)
    T = "ababaca"
    P = "ababaca"
    AF = AFConstruct(P, bases)
    t_ini = time()
    #ix,cmp = brute_force(T,P)
    ix = AFMatch(T, P, AF, bases)
    t_fim = time() - t_ini
    #print("Busca por força bruta de %d elementos encontrado no ix %d em %.6f segundos com %d comparacoes"%(len(T),ix,t_fim,cmp))
    print("Busca por AF de %d elementos encontrado no ix %d em %.6f segundos"%(len(T),ix,t_fim))
    print("Texto: {}".format(T))
    print("Padrao: {}".format(P))
    pass