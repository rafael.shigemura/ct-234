import math

nos = 0

class no:
    def __init__(self):
        self.esq = None
        self.dir = None


def montaABB(n):
    if n==1: return no()
    nozinho = no()
    nozinho.esq = montaABB(n//2)
    nozinho.dir = montaABB(n//2)
    return nozinho


def contaNos(raiz):
    global nos
    if raiz is None: return 
    nos += 1
    if raiz.esq:
        contaNos(raiz.esq)
    if raiz.dir:
        contaNos(raiz.dir)


def ehCompleta(raiz):
    global nos
    ret = False
    contaNos(raiz)
    if math.log(nos+1,2) % 1 == 0:
        ret = True
    return ret


if __name__ == "__main__":
    n = 15
    raiz = montaABB(n)
    if ehCompleta(raiz):
        print("Arvore de %d no(s) é completa." % n)
    else:
        print("Arvore de %d no(s) não é completa." % n)
    #print("MergeSort de %s em %s calculado em %.0f segundos "%(va, v, t_fim))
    pass
