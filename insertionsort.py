from time import time
import numpy
import copy

global arr 

def inicializa_array(n):
    return numpy.random.randint(n, size=n)

def insertionsort(n, arr):
    v = copy.deepcopy(arr)
    
    for i in range(1,n):            # LAÇO MAIS EXTERNO PARA PERCORRER O VETOR DE 1 A n-1 (INICIA DO SEGUNDO ELEMENTO)
        x = v[i]                    # ASSUME COMO O PRIMEIRO DA RODADA O SEGUNDO ELEMENTO DO VETOR
        j = i                       # INICIALIZAÇÃO DO CONTADOR DO LAÇO MAIS INTERNO, SENDO IGUAL O CONTADOR DO LAÇO MAIS EXT
        while j>0 and  x < v[j-1]:  # LAÇO MAIS INTERNO, REVERSO, DE i ATÉ 1, COM A CONDIÇÃO QUE O MENOR VALOR SUPORTO SEJA MENTOR QUE O ANTERIOR
            v[j] = v[j-1]           # DESLOCA OS ELEMENTOS PARA A DIREITA PARA ENCAIXAR O X, MENOR QUE ELES
            j -= 1                  # DECREMENTO DO CONTADOR DO LAÇO MAIS INTERNO    
        v[j] = x                    # AO FINAL, ATRIBUI AO VETOR NA POSIÇÃO CORRETA O VALOR X ORDENADO
    
    return v

if __name__ == "__main__":
    n = 5
    arr = inicializa_array(n)
    t_ini = time()
    arr_ord = insertionsort(n,arr)
    t_fim = time() - t_ini
    print("Selectionsort de %d elementos calculado em %.6f segundos"%(n,t_fim))
    pass