import copy
from math import floor
from time import time
import numpy


global va
global v
global aux


def inicializa_array(n, seq=False, val=None):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    if seq: v = [i for i in range(n)]
    elif val: v = val
    else: v = numpy.random.randint(n, size=n)
    va = copy.deepcopy(v)


def inicializa_array_seq(n):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    v = [i for i in range(n)]
    va = copy.deepcopy(v)


# Python program for implementation of heap Sort
 
# To heapify subtree rooted at index i.
# n is size of heap
 
 
def heapify(arr, n, i):
    largest = i  # Initialize largest as root
    l = 2 * i + 1     # left = 2*i + 1
    r = 2 * i + 2     # right = 2*i + 2
 
    # See if left child of root exists and is
    # greater than root
    if l < n and arr[largest] < arr[l]:
        largest = l
 
    # See if right child of root exists and is
    # greater than root
    if r < n and arr[largest] < arr[r]:
        largest = r
 
    # Change root, if needed
    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]  # swap
 
        # Heapify the root.
        heapify(arr, n, largest)
 
# The main function to sort an array of given size
 
 
def heapsort(arr):
    n = len(arr)
 
    # Build a maxheap.
    for i in range(n//2 - 1, -1, -1):
        heapify(arr, n, i)
 
    # One by one extract elements
    for i in range(n-1, 0, -1):
        arr[i], arr[0] = arr[0], arr[i]  # swap
        heapify(arr, i, 0)
 

if __name__ == "__main__":
    n = 5
    inicializa_array(n)
    t_ini = time()
    heapsort(v)
    t_fim = time() - t_ini
    print("heapsort de %s em %s calculado em %.0f segundos "%(va, v, t_fim))
    pass