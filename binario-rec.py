from time import time

b = ''

def bin(n):
    global b
    if(n // 2 == 0): 
        b += str(n)
    else:
        bin(n // 2)
        b += str(n % 2)
    

if __name__ == "__main__":
    n = 123456789123456789
    t_ini = time()
    bin(n)
    t_fim = time() - t_ini
    print(b)
    print("Conversão recursiva realizada em %f segundos"%(t_fim))
    pass