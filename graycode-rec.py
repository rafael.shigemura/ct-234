from time import time

def graycode(n):
    if n == 0:
        return ['']
    fh = graycode(n-1)
    sh = fh.copy()
    fh = ['0' + cd for cd in fh]
    sh = ['1' + cd for cd in reversed(sh)]

    return fh + sh


if __name__ == "__main__":
    n = 2
    t_ini = time()
    codes = graycode(n)
    t_fim = time() - t_ini
    for code in codes:
        print(code)
    print("Conversão recursiva realizada em %f segundos"%(t_fim))
    pass
