
# w é capacidade da mochila,
# wt é o peso dos itens,
# val são os lucros
# n é len(wt)

def mochila(w, wt, val, n):
    if n == 0 or w == 0:
        return 0
    if memo[n][w] != -1:
        return memo[n][w]
    if wt[n-1] <= w:
        memo[n][w] = max(val[n-1] + mochila(w-wt[n-1], wt, val, n-1), mochila(w, wt, val, n-1))
        return memo[n][w]
    elif wt[n-1] > w:
        memo[n][w] = mochila(w, wt, val, n-1)
        return memo[n][w]

memo = [[-1 for i in range(11)] for j in range(5)]
res = mochila(10, [2, 3, 5, 8], [10, 15, 70, 80], 4)
res