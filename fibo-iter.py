from time import time


def fib(n):
    if(n==0 or n==1): return 1
    f1 = 1
    f2 = 1
    f3 = 0
    for i in range(2,n+1):
        f3 = f1 + f2
        f1 = f2
        f2 = f3
    return f3


if __name__ == "__main__":
    n = 40
    t_ini = time()
    n_fib = fib(n)
    t_fim = time() - t_ini
    print("Fibo iter de %d é %d calculado em %.0f segundos"%(n,n_fib,t_fim))