def DCMakeChangeMemo(moedas, troco):
    if (troco == 0): return 0;
    #print(memo)
    if memo[troco] < 0:       
        q = troco; # solução óbvia com somente moedas de 1 centavo
        for i in range(len(moedas)):
            if (moedas[i] > troco): continue #essa moeda não serve
            memo[troco] = min(q, 1 + DCMakeChangeMemo(moedas, troco - moedas[i]))
    return memo[troco]


troco = 999 #LIMITE
memo = [-1 for i in range(troco+1)]
DCMakeChangeMemo([1,2,3,6], troco) 