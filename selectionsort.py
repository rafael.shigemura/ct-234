from time import time
import numpy
import copy

global arr 

def inicializa_array(n):
    return numpy.random.randint(n, size=n)

def selectionsort(n, arr):
    v = copy.deepcopy(arr)

    for i in range(n):          # LAÇO EXTERNO PARA PERCORRER DE 0 A n-1
        min = i                 # ASSUMA QUE O ÍNDICE i DO MENOR É O PRIMEIRO ELEMENTO DA RODADA
        for j in range(i+1,n):  # NÓ MAIS INTERNO PARA PERCORRER DE ÍNDICE i+1 ATÉ n-1
            if v[j] < v[min]:   # SE O VALOR EM PASSANT FOR MENOR QUE O MÍNINO APURADO NA RODADA...
                min = j         # ANOTE O NOVO ÍNDICE j DO NOVO MÍNIMO
        x = v[min]              # APÓS O TÉRMINO DO LAÇO, ANOTE O VALOR DO NOVO MÍNIMO
        v[min] = v[i]           # ARMAZENE O SUPOSTO MINIMO DA PROXIMA RODADA
        v[i] = x                # CONSOLIDE O MÍNIMO ENCONTRADO ATÉ O MOMENTO
    
    return v

if __name__ == "__main__":
    n = 4
    arr = inicializa_array(n)
    t_ini = time()
    arr_ord = selectionsort(n, arr)
    t_fim = time() - t_ini
    print("Selectionsort de %d elementos calculado em %.6f segundos"%(n,t_fim))
    pass