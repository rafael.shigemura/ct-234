from re import I
from time import time
import numpy

arr = None

def inicializa_array(n):
    return [i for i in range(n)]

def inicializa_array_bin(n):
    return [1 for _ in range(n)]


def jos4(n,s):
    m = 0
    k = 0    
    while True:
        ka = k
        arr_bin[(k+s)%n] = 0
        m += 1
        if m == (n-1): break
        k = (k+2*s)%n
        if k <= ka:
            s = s*2
    
    return k+1


def jos3(n,k):
    i=0
    m = -1
    d=1
    while arr_bin[i]>0 and arr_bin[(i+d)%n]>0:
        m = i 
        arr_bin[(i+d)%n]=0
        i=(i+k)%n
        if i==0: 
            d+=1
    return m+1

def jos(n,p):
    prox = (p+1) % n
    if arr[prox]==-1: 
        return arr[p]
    arr[prox] = -1
    if arr[(p+2) % n] == -1:
        print("%d matou %d e acabou o jogo."%(p+1,prox+1))
        return arr[p]
    print("%d matou %d e passou a faca para %d"%(p+1,prox+1,(p+2) % n+1))
    return jos(n,(p+2) % n) + 1

def jos2(n , k):
    if n==1:
        return 1
    else:
        return (jos2(n-1 , k) + k-1) % n +1

if __name__ == "__main__":
    n = 10
    k = 2
    arr = inicializa_array(n)
    arr_bin = inicializa_array_bin(n)

    t_ini = time()
    j2 = jos2(n,k)
    t_fim = time() - t_ini
    print("O valor %d é o nr de Josephus 2 para entrada %d. Algoritmo realizado em %.0f segundos"%(j2,n,t_fim))

    t_ini = time()
    j3 = jos4(n,1)
    t_fim = time() - t_ini
    print("O valor %d é o nr de Josephus 4 para entrada %d. Algoritmo realizado em %.0f segundos"%(j3,n,t_fim))
    pass