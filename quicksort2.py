import copy
from math import floor
from time import time
import numpy


global va
global v
global aux


def inicializa_array(n, seq=False, val=None):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    if seq: v = [i for i in range(n)]
    elif val: v = val
    else: v = numpy.random.randint(n, size=n)
    va = copy.deepcopy(v)

''' Python implementation of QuickSort using Hoare's
partition scheme. '''
 
''' This function takes first element as pivot, and places
      all the elements smaller than the pivot on the left side
      and all the elements greater than the pivot on
      the right side. It returns the index of the last element
      on the smaller side '''
 
 
def partition(arr, low, high):
 
    pivot = arr[low]
    i = low - 1
    j = high + 1
 
    while (True):
 
        # Find leftmost element greater than
        # or equal to pivot
        i += 1
        while (arr[i] < pivot):
            i += 1
 
        # Find rightmost element smaller than
        # or equal to pivot
        j -= 1
        while (arr[j] > pivot):
            j -= 1
 
        # If two pointers met.
        if (i >= j):
            return j
 
        arr[i], arr[j] = arr[j], arr[i]
  
 
''' The main function that implements QuickSort
arr --> Array to be sorted,
low --> Starting index,
high --> Ending index '''
 
 
def quickSort(arr, low, high):
    ''' pi is partitioning index, arr[p] is now
    at right place '''
    if (low < high):
 
        pi = partition(arr, low, high)
 
        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low, pi)
        quickSort(arr, pi + 1, high)
 

if __name__ == "__main__":
    n = 4
    inicializa_array(n, val=[7, 9, 0, 8])
    t_ini = time()
    quickSort(v, 0, n-1)
    t_fim = time() - t_ini
    print("MergeSort de %s em %s calculado em %.0f segundos "%(va, v, t_fim))
    pass