from time import time
import numpy

def inicializa_array(n):
    return numpy.random.randint(10, size=n)

def inv(n):
    if (n==0): return arr[n]
    print(arr[n-1])
    inv(n-1)

if __name__ == "__main__":
    n = 10
    arr = inicializa_array(n)
    t_ini = time()
    inv(n)
    t_fim = time() - t_ini
    print("Vetor original: %s"%arr)
    print("Inversão realizada em %.0f segundos"%(t_fim))
    pass