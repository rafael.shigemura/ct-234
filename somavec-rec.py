from time import time
import numpy

def inicializa_array(n):
    return numpy.random.randint(10, size=n)

def soma(n):
    if (n==1): return arr[n-1]
    return arr[n-1] + soma(n-1)

if __name__ == "__main__":
    n = 10
    arr = inicializa_array(n)
    t_ini = time()
    s = soma(n)
    t_fim = time() - t_ini
    print("A soma do vetor %s é %d. Busca realizada em %.0f segundos"%(str(arr),s,t_fim))
    pass