import copy
from math import floor
from time import time
import numpy


global va
global v
global aux


def inicializa_array(n):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    v = numpy.random.randint(n, size=n)
    va = copy.deepcopy(v)
    return v, aux


def inicializa_array_seq(n):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    v = [i for i in range(n)]
    va = copy.deepcopy(v)


def merge(A, aux, esquerda, meio, direita):
    """
    Combina dois vetores ordenados em um único vetor (também ordenado).
    """
    for k in range(esquerda, direita + 1):
        aux[k] = A[k]
    i = esquerda
    j = meio + 1
    for k in range(esquerda, direita + 1):
        if i > meio:
            A[k] = aux[j]
            j += 1
        elif j > direita:
            A[k] = aux[i]
            i += 1
        elif aux[j] < aux[i]:
            A[k] = aux[j]
            j += 1
        else:
            A[k] = aux[i]
            i += 1


def mergesort(A, aux, esquerda, direita):
    if direita <= esquerda:
        return
    meio = (esquerda + direita) // 2

    # Ordena a primeira metade do arranjo.
    mergesort(A, aux, esquerda, meio)

    # Ordena a segunda metade do arranjo.
    mergesort(A, aux, meio + 1, direita)

    # Combina as duas metades ordenadas anteriormente.
    merge(A, aux, esquerda, meio, direita)


if __name__ == "__main__":
    n = 4
    inicializa_array(n)
    t_ini = time()
    mergesort(v, aux, 0, n-1)
    t_fim = time() - t_ini
    print("MergeSort de %s em %s calculado em %.0f segundos "%(va, v, t_fim))
    pass