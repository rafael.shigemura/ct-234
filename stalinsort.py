import copy
from math import floor
from time import time
import numpy


global va
global v
global aux


def inicializa_array(n, seq=False, val=None):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    if seq: v = [i for i in range(n)]
    elif val: v = val
    else: v = numpy.random.randint(n, size=n)
    va = copy.deepcopy(v)


def inicializa_array_seq(n):
    global v
    global aux
    global va
    aux = numpy.zeros(n,dtype=numpy.uint)
    v = [i for i in range(n)]
    va = copy.deepcopy(v)


def stalinsort(l):
    max_val = l[0]

    def add_val(num):
        nonlocal max_val
        max_val = num
        return num

    return [add_val(x) for x in l if x >= max_val]


if __name__ == "__main__":
    n = 100000
    inicializa_array(n)
    t_ini = time()
    v = stalinsort(v)
    t_fim = time() - t_ini
    print("StalinSort de %d elementos calculado em %.6f segundos. Morreram %d e restaram %d!!"%(n,t_fim, len(v), n-len(v)))
    pass