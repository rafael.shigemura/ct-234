from time import time

def bin(n):
    bn =''
    while (n // 2 > 0):
        bn += str(n % 2)
        n = n // 2
    bn += str(n)
    return bn[::-1]
    

if __name__ == "__main__":
    n = 123456789123456789
    t_ini = time()
    bnr = bin(n)
    t_fim = time() - t_ini
    print(bnr)
    print("Conversão iterativa realizada em %f segundos"%(t_fim))
    pass