from time import time
import numpy

arr = None
v = None
existe = False


def inicializa_array(n):
    return numpy.random.randint(10, size=n)

def exists(n):
    global v
    global existe
    if (n==1): return arr[n-1]
    if (v==exists(n-1)): 
        existe = True
        return
    return arr[n-1] 

if __name__ == "__main__":
    n = 10
    v = 5
    arr = inicializa_array(n)
    t_ini = time()
    exists(n)
    t_fim = time() - t_ini
    print("O valor %d tem existência %s no vetor %s. Busca realizada em %.0f segundos"%(v,existe,str(arr),t_fim))
    pass