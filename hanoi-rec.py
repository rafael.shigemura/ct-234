from time import time

def hanoi(n, org, dest, aux):
    if n==1:
        print("Mova de %s para %s"%(org,dest))
    else:
        hanoi(n-1, org, aux, dest)
        print("Mova de %s para %s"%(org,dest))
        hanoi(n-1, aux, dest, org)


if __name__ == "__main__":
    n = 2
    t_ini = time()
    hanoi(n,1,3,2)
    t_fim = time() - t_ini
    print("Inversão realizada em %.0f segundos"%(t_fim))
    pass