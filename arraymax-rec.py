from time import time
import numpy

global arr

def inicializa_array(n):
    return numpy.random.rand(n)

def arrmax(n):
    if (n==1): return arr[n-1]
    max = arrmax(n-1)
    if (arr[n-1]>max): max = arr[n-1]
    return max

if __name__ == "__main__":
    n = 10
    arr = inicializa_array(n)
    t_ini = time()
    max = arrmax(n)
    t_fim = time() - t_ini
    print("Max de %d elementos é %.2f calculado em %.0f segundos"%(n,max,t_fim))
    pass